#!/usr/bin/env python3
import logging.config

from lnregtestnet.lib.network import RegtestNetwork
from lnregtestnet.lib.common import logger_config

logging.config.dictConfig(logger_config)


testnet = RegtestNetwork(
    from_scratch=True, node_limit='C', network_definition='star_ring'
)

testnet.run_continuously()
