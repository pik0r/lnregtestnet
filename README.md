LN regtest network
==================

Implements a lightning network for (python) integration testing based on a 
bitcoin regtest network.

The simulated lightning network's size can have different shapes as defined
 in the `network_definitions` folder.

An example of a graph (`star_ring`) with 7 nodes and 12 channels is shown 
here (for channel balance details, have a look at `network_definitions.star_ring`):
```
Star component:
A -> B
A -> C
A -> D
A -> E
A -> F
A -> G
Ring component:
B -> C
C -> D
D -> E
E -> F
F -> G
G -> B
```

Features
----------------
* No external python dependencies
* Arbitrary lightning network graphs of size ~10 (dependent on your ressources)
* LND support
* Lightning graph state comparison
* Restarting from already created networks
* Abstraction of random identifiers (public keys, channel ids) to memorizable
  identifiers

Create own network topology
---------------------------
Networks of arbitrary shape can be defined as a python dictionary in the
`network_definitions` folder. The requirements for such definitions can be
seen from the given templates and altered, but they should go with the
following rules:
* Node A is the master node
* Nodes are uniquely named by subsequent characters A, B, C, ...
* Channels are uniquely numbered by incremental integers 1, 2, 3, ...
* Ports must be set differently

Testing other python projects
-----------------------------
Samples of how one can test python code can be found in the `test` folder
and more information can be found in [TEST](./test/TEST.md).
The main feature is to compare the state (including balances) of the
simulated lightning network before and after some action by employing the 
`assemble_graph` method and comparing the graphs via `lib.utils.dict_comparison.`

Planned features
----------------
* Arbitrary lightning daemon binary detection (lnd, clightning, ...)
* Time-dependent transaction series testing

Setup
-----
* **In order to have a complete immediate graph view, we need to compile lnd
in a special way by setting to `defaultTrickleDelay = 1` in `config.go`.**

* bitcoind, bitcoin-cli, lnd, and lncli are expected to be found in `$PATH`

* To run the network:
  ```
  $ python3 run_network.py
  ```
* To clean up the project folder:
  ```
  $ ./contrib/cleanup.sh
  ```

Testing
-------
To run all tests, run
`python3 -m unittest discover test` from the root folder.

Troubleshooting
---------------

The simulation of a lightning network is memory and CPU intensive. Each LN
node needs some time to get up and running and consumes ressources.
Currently, the startup of each lnd node is delayed to distribute CPU load.
 If you experience startup problems, increase `LOAD_BALANCING_LND_STARTUP_TIME_SEC` in
`lib.common`.
