import re, setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('./lnregtestnet/__init__.py', 'r') as f:
    MATCH_EXPR = "__version__[^'\"]+(['\"])([^'\"]+)"
    VERSION = re.search(MATCH_EXPR, f.read()).group(2)

# to package, run: python3 setup.py sdist bdist_wheel
setuptools.setup(
    name="lnregtestnet-test",
    version=VERSION,
    author="bitromortac",
    author_email="bitromortac@protonmail.com",
    description="Bitcoin regtest Lightning Network for integration testing.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/bitromortac/lnregtestnet",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)